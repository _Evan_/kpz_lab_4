﻿using Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KPZ_Lab_4.Context
{
    public class StorageContext:DbContext
    {
        public StorageContext(DbContextOptions options): base(options)
        {

        }
        public virtual DbSet<Storage> Storages { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<StorageProduct> StorageProducts { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<StorageProduct>()
              .HasKey(t => new { t.StorageId, t.ProductId });

            modelBuilder.Entity<StorageProduct>()
                .HasOne(p => p.Storage)
                .WithMany(x => x.StorageProducts)
                .HasForeignKey(y => y.StorageId);

            modelBuilder.Entity<StorageProduct>()
               .HasOne(p => p.Product)
               .WithMany(x => x.StorageProducts)
               .HasForeignKey(y => y.ProductId);


        }
    }
}
