﻿using KPZ_Lab_4.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KPZ_Lab_4.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly StorageContext _context;
        private DbSet<TEntity> _entities;

        public Repository(StorageContext context)
        {
            this._context = context;
        }

        private DbSet<TEntity> Entities
        {
            get
            {
                if (_entities == null)
                {
                    _entities = _context.Set<TEntity>();
                }

                return _entities as DbSet<TEntity>;
            }
        }

        public virtual IQueryable<TEntity> Table
        {
            get
            {
                return this.Entities;
            }
        }

        public virtual TEntity Get(int id)
        {
            return this.Entities.Find(id);
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return this.Entities;
        }

        public virtual IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate)
        {
            return this.Entities.Where(predicate).ToList();
        }

        public virtual TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return this.Entities.FirstOrDefault(predicate);
        }

        public virtual TEntity Add(TEntity entity)
        {
            this.Entities.Add(entity);
            this._context.SaveChanges();

            return entity;
        }

        public virtual void Update(TEntity entity)
        {
            this.Entities.Update(entity);
            this._context.SaveChanges();
        }

        public virtual void Remove(TEntity entity)
        {
            this.Entities.Remove(entity);
            this._context.SaveChanges();
        }
    }
}
