﻿using Data.Models;
using KPZ_Lab_4.DTO;
using KPZ_Lab_4.Repository;
using KPZ_Lab_4.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KPZ_Lab_4.Services
{
    public class ProductService : IProductService
    {
        private readonly IRepository<Storage> _storageRepository;
        private readonly IRepository<StorageProduct> _storageProductRepository;
        private readonly IRepository<Product> _productRepository;

        public ProductService(IRepository<Storage> storageRepository, IRepository<Product> productRepository, IRepository<StorageProduct> storageProductRepository)
        {
            _storageRepository = storageRepository;
            _productRepository = productRepository;
            _storageProductRepository = storageProductRepository;
        }

        public IEnumerable<ProductDto> GetAll(bool joinStorage)
        {
            try
            {
                List<ProductDto> listOfDto = new List<ProductDto>();
                List<Product> products = new List<Product>();

                if (joinStorage)
                    products = _productRepository.Table.Include(p => p.StorageProducts).OrderByDescending(p => p.ProductId).ToList();
                else
                    products = _productRepository.Table.OrderByDescending(p => p.ProductId).ToList();
                foreach (var product in products)
                {
                    ProductDto productDto = new ProductDto(product);

                    if (joinStorage)
                    {
                        List<StorageDto> temp = new List<StorageDto>();

                        foreach (var storage in product.StorageProducts)
                            temp.Add(new StorageDto(storage.Storage));

                        productDto.Storages = temp;
                    }
                    listOfDto.Add(productDto);
                }
                return listOfDto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void Insert(ProductDto productDto)
        {
            Product product = new Product()
            {
                Name = productDto.Name,
                Price = productDto.Price,
            };

            _productRepository.Add(product);
        }
        public void Update(ProductDto productDto)
        {

        }
        public bool Remove(int id)
        {
            return true;
        }

    }
}
