﻿using KPZ_Lab_4.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KPZ_Lab_4.Services.Interfaces
{
    public interface IProductService
    {
        IEnumerable<ProductDto> GetAll(bool joinStorage);
        //ProductDto GetAll(int page, string filterBy = null, bool joinStorage = false);
        //ProductDto Get(int id, bool joinStorage);
        void Insert(ProductDto productDto);
        void Update(ProductDto productDto);
        //void Remove(ProductDto productDto);
        //void Remove(int id);
    }
}
