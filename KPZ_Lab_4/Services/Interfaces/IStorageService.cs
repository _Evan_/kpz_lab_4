﻿using KPZ_Lab_4.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KPZ_Lab_4.Services.Interfaces
{
    public interface  IStorageService
    {
        IEnumerable<StorageDto> GetAll(bool joinProduct);
        //ProductDto GetAll(int page, string filterBy = null, bool joinProduct = false);
        //ProductDto Get(int id, bool joinProduct);
        void Insert(StorageDto storage);
        void Update(StorageDto storage);
       // void Remove(StorageDto storage);
     //   void Remove(int id);
    }
}
