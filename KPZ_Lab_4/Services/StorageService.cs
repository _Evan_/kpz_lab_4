﻿using Data.Models;
using KPZ_Lab_4.DTO;
using KPZ_Lab_4.Repository;
using KPZ_Lab_4.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KPZ_Lab_4.Services
{
    public class StorageService : IStorageService
    {
        private readonly IRepository<Storage> _storageRepository;
        private readonly IRepository<StorageProduct> _storageProductRepository;
        private readonly IRepository<Product> _productRepository;

        public StorageService(IRepository<Storage> storageRepository, IRepository<Product> productRepository, IRepository<StorageProduct> storageProductRepository)
        {
            _storageRepository = storageRepository;
            _productRepository = productRepository;
            _storageProductRepository = storageProductRepository;
        }
        public IEnumerable<StorageDto> GetAll(bool joinProducts)
        {
            List<StorageDto> storages = new List<StorageDto>();
            return storages;
        }
        public void Insert(StorageDto storageDto)
        {
            Storage storage = new Storage()
            {
                Address = storageDto.Address
            };

            _storageRepository.Add(storage);
        }
        public void Update(StorageDto storage)
        {

        }
        public void Remove(int id)
        {
           
        }
    }
}
