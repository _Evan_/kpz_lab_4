﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class Storage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int StorageId { get; set; }
        public string Address { get; set; }

        public ICollection<StorageProduct> StorageProducts { get; set; }
        public Storage()
        {
            StorageProducts = new Collection<StorageProduct>();
        }
    }
}
