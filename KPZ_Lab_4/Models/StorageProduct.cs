﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class StorageProduct
    {
        public int StorageId { get; set; }
        public Storage Storage { get; set; }

        public int ProductId { get; set; }
        public Product Product { get; set; }
    }
}
