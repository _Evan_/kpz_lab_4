﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public enum State
    {
        Good,
        Bad,
        satisfactory,
        Normal
    }
    public class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public State State { get; set; }
        public int Amount { get; set; }
        public float Price { get; set; }

        public ICollection<StorageProduct> StorageProducts { get; set; }
        public Product()
        {
            StorageProducts = new Collection<StorageProduct>();
        }
    }
}
