﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KPZ_Lab_4.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace KPZ_Lab_4.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _service;
        public ProductController(IProductService service)
        {
            _service = service;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get(bool joinStorage)
        {
            var result = _service.GetAll(joinStorage).ToList();
            if (result.Count == 0)
                return NoContent();
            else 
                return Ok(result);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
