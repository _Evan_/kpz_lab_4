﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KPZ_Lab_4.DTO
{
    public class ProductDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }
        public int Amount { get; set; }
        public ICollection<StorageDto> Storages { get; set; }

        public ProductDto(Product product)
        {
            Id = product.ProductId;
            Name = product.Name;
            Description = product.Description;
            Amount = product.Amount;
            Price = product.Price;
        }
    }
}
