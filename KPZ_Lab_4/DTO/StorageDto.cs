﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KPZ_Lab_4.DTO
{
    public class StorageDto
    {
        public int Id { get; set; }
        public string Address { get; set; }

        public ICollection<ProductDto> Products { get; set; }

        public StorageDto(Storage storage)
        {
            Id = storage.StorageId;
            Address = storage.Address;
        }
    }
}
